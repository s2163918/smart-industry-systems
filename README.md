
# Mosquitto MQTT Broker Setup and Integration with Python

This guide provides step-by-step instructions on how to set up the Mosquitto MQTT broker on Windows, how to use it with a Python application, and how to link it with an OPC UA server. Furthermore, the transform.py file shows the transformation of the raw data (TDMS format) to a Knowledge Graph using the SAREF ontology.

## Prerequisites

- Python installed (version 3.6 or higher)
- pip installed (Python package installer)
- OPC UA Server running
- Mosquitto installed and configured

## Step 1: Install Mosquitto on Windows

1. **Download the installer:**
   - Go to the [Eclipse Mosquitto download page](https://mosquitto.org/download/) and download the Windows installer.

2. **Run the installer:**
   - Follow the installation prompts to install Mosquitto. The default installation directory is `C:\Program Files\mosquitto`.

3. **Add Mosquitto to the System PATH:**
   - Open the Start Menu and search for "Environment Variables".
   - Click on "Edit the system environment variables".
   - In the System Properties window, click on the "Environment Variables..." button.
   - In the Environment Variables window, find the "Path" variable in the "System variables" section and select it. Click "Edit...".
   - In the Edit Environment Variable window, click "New" and add the path to the Mosquitto installation directory (e.g., `C:\Program Files\mosquitto`).
   - Click "OK" to close all windows.

4. **Configure the Mosquitto Service:**
   - Open a Command Prompt as Administrator and start the Mosquitto service:
     ```cmd
     net start mosquitto
     ```
   - To stop the service, use:
     ```cmd
     net stop mosquitto
     ```

## Step 2: Configure Mosquitto (Optional)

1. **Locate the configuration file:**
   - The default configuration file is located at `C:\Program Files\mosquitto\mosquitto.conf`.

2. **Edit the configuration file (if needed):**
   - Open the configuration file in a text editor (like Notepad) and make any necessary changes. For example, to allow anonymous access (useful for testing):
     ```ini
     allow_anonymous true
     ```

3. **Restart the Mosquitto service:**
   - After making changes to the configuration file, restart the Mosquitto service to apply the changes:
     ```cmd
     net stop mosquitto
     net start mosquitto
     ```

## Step 3: Set Up Python Environment

1. **Install required Python packages:**
   - Open a Command Prompt and run:
     ```cmd
     pip install paho-mqtt opcua
     ```

2. **Create a Python script (opcua_to_mqtt.py):**
   - Create a new file named `opcua_to_mqtt.py` and add the following code:
     ```python
     import time
     import paho.mqtt.client as mqtt
     from opcua import Client

     # MQTT broker details
     mqtt_broker = "localhost"
     mqtt_port = 1883
     mqtt_topic = "opcua/data"

     # OPC UA server details
     opcua_server_url = "opc.tcp://localhost:4840"

     # Function to connect to MQTT broker
     def on_connect(client, userdata, flags, rc):
         print(f"Connected to MQTT broker with result code {rc}")

     # Create MQTT client and connect
     mqtt_client = mqtt.Client()
     mqtt_client.on_connect = on_connect
     mqtt_client.connect(mqtt_broker, mqtt_port, 60)

     # Connect to OPC UA server
     opcua_client = Client(opcua_server_url)
     opcua_client.connect()

     # Function to read data from OPC UA server and publish to MQTT
     def publish_opcua_data():
         try:
             # Replace 'NodeID' with the actual node ID you want to read from OPC UA server
             node = opcua_client.get_node("ns=2;i=2")
             value = node.get_value()
             print(f"Read value from OPC UA: {value}")

             # Publish to MQTT
             mqtt_client.publish(mqtt_topic, payload=value, qos=0, retain=False)
             print(f"Published to MQTT: {value}")
         except Exception as e:
             print(f"Error: {e}")

     try:
         while True:
             publish_opcua_data()
             time.sleep(5)  # Adjust the sleep time as needed
     except KeyboardInterrupt:
         print("Interrupted by user")

     # Disconnect from OPC UA server
     opcua_client.disconnect()
     ```

3. **Run the script:**
   - Open a Command Prompt, navigate to the directory where `opcua_to_mqtt.py` is located, and run:
     ```cmd
     python opcua_to_mqtt.py
     ```

4. **Verify:**
   - Subscribe to the MQTT topic to verify that data is being published:
     ```cmd
     mosquitto_sub -h localhost -t opcua/data
     ```

   - The subscribed terminal should display the data being published from the OPC UA server.

## Additional Tips

- **Security:** For production environments, configure authentication and encryption (TLS/SSL) in the Mosquitto configuration file.
- **Logging:** Check Mosquitto logs for troubleshooting. Logs can be enabled and directed to a file or the Windows event log via the configuration file.

## Running pubsub

1. **Ensure Mosquitto is running:**
   - Follow the steps mentioned above to start the Mosquitto service if it is not already running.

2. **Run the pubsub script:**
   - Open a Command Prompt, navigate to the directory where `publisher.py` is located, and run:
     ```cmd
     python publisher.py
     ```

   - This script will publish messages to the specified MQTT topic.

3. **Verify:**
   - Open another Command Prompt to subscribe to the topic and verify the published messages:
     ```cmd
     mosquitto_sub -h localhost -t your_topic
     ```

## Running SIS_SIM

1. **Run the SIS_SIM script:**
   - Open a Command Prompt, navigate to the directory where `SIS_SIM_V2.py` is located, and run:
     ```cmd
     python SIS_SIM_V2.py
     ```
