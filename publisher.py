import pandas as pd
from nptdms import TdmsFile
import paho.mqtt.client as mqtt
import time
import json

# Adjustable variables
data_points_per_second = 100  # Target data points per second for downsampling
mqtt_broker = "localhost"  # MQTT broker address
mqtt_port = 1883  # MQTT broker port
mqtt_topic = "tdms/data"  # MQTT topic to publish to
tdms_file_path = r'./t_0001.tdms'  # Path to your TDMS file

# Part 1: Load TDMS and create DataFrame
tdms_file = TdmsFile.read(tdms_file_path)
group = tdms_file['Untitled']  # Replace 'Untitled' with the actual group name
df = group.as_dataframe()

print('TDMS file loaded')

# Part 2: Downsample the data
# Assuming 'Time' is the column with datetime information
df['Time'] = pd.to_datetime(df['Time'])
df.set_index('Time', inplace=True)
df_downsampled = df.resample(f'{int(1000 / data_points_per_second)}L').mean()  # Convert ms to frequency string

# Part 3: Initialize MQTT client
mqtt_client = mqtt.Client()

# Connect to the MQTT broker
mqtt_client.connect(mqtt_broker, mqtt_port, 60)

# Function to send data with timing
def send_data(data):
    send_interval = 1 / data_points_per_second  # Calculate the time interval between sends
    for _, row in data.iterrows():
        payload = row.to_json()
        mqtt_client.publish(mqtt_topic, payload)
        print(f"Published: {payload}")
        time.sleep(send_interval)  # Wait for the time interval before sending the next data point

# Start the MQTT client loop
mqtt_client.loop_start()

# Example: Send downsampled data
send_data(df_downsampled)

# Stop the MQTT client loop after sending all data
mqtt_client.loop_stop()

print("Data sent successfully")
