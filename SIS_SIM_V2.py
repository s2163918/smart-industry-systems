import glfw
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import numpy as np
import json
import time
import asyncio
import paho.mqtt.client as mqtt
import scipy.integrate as integrate
from scipy.signal import butter, filtfilt

# Define window parameters
window_width, window_height = 800, 600

# Define the number of floors and their dimensions
num_floors = 5
floor_width = 1
floor_depth = 1
floor_height = 1

# Define the initial y-position of the rectangles
initial_y_position = 0

# Define the position of the rectangles along the y-axis
y_positions = np.arange(num_floors) * (floor_height) + initial_y_position

initial_x_position = 0

# Define the position of the rectangles along the x-axis
x_positions = np.zeros(num_floors) + initial_x_position

initial_z_position = 0

# Define the position of the rectangles along the z-axis
z_positions = np.zeros(num_floors) + initial_z_position

rotation_angles = np.zeros(num_floors)

# Define node positions relative to each rectangle
nodes_per_floor = 4
node_positions = np.zeros((num_floors, nodes_per_floor, 3))

for i in range(num_floors):
    node_positions[i][0] = [initial_x_position, y_positions[i], initial_z_position]
    node_positions[i][1] = [initial_x_position + floor_width, y_positions[i], initial_z_position]
    node_positions[i][2] = [initial_x_position, y_positions[i], initial_z_position + floor_depth]
    node_positions[i][3] = [initial_x_position + floor_width, y_positions[i], initial_z_position + floor_depth]

# MQTT connection details
mqtt_broker = "localhost"  # Change this to your MQTT broker address
mqtt_port = 1883
mqtt_topic = "tdms/data"  # Change this to your MQTT topic

# Buffers to store last acceleration data for each axis
acceleration_buffer_size = 10
acceleration_buffers_x = [[0] * acceleration_buffer_size for _ in range(num_floors)]
acceleration_buffers_y = [[0] * acceleration_buffer_size for _ in range(num_floors)]
acceleration_buffers_z = [[0] * acceleration_buffer_size for _ in range(num_floors)]

time_data = []

# High-pass filter parameters
sampling_rate = 100  # Hz
cutoff_frequency = 0.1  # Hz
b, a = butter(1, cutoff_frequency / (0.5 * sampling_rate), btype='high')

def highpass_filter(data):
    return filtfilt(b, a, data)

# MQTT callbacks
def on_connect(client, userdata, flags, rc):
    client.subscribe(mqtt_topic)

def on_message(client, userdata, msg):
    message_handler(msg.payload.decode())

# Initialize MQTT client
mqtt_client = mqtt.Client(protocol=mqtt.MQTTv311)  # Use the latest MQTT protocol version
mqtt_client.on_connect = on_connect
mqtt_client.on_message = on_message
mqtt_client.connect(mqtt_broker, mqtt_port, 60)

# Calculate initial distances between corresponding nodes on adjacent floors
initial_distances = np.zeros((num_floors - 1, nodes_per_floor))
for floor in range(num_floors - 1):
    for node in range(nodes_per_floor):
        initial_distances[floor][node] = np.linalg.norm(node_positions[floor][node] - node_positions[floor + 1][node])

def message_handler(message):
    try:
        data = json.loads(message)
        update_node_positions(data)
    except json.JSONDecodeError:
        pass

def update_node_positions(data):
    global node_positions, rotation_angles, time_data

    if not time_data:
        time_data.append(0)
    else:
        time_data.append(time_data[-1] + 0.01)

    for key, value in data.items():
        if key.startswith("A"):
            sensor_id = key[:-1]  # Extract sensor ID
            axis = key[-1]  # Extract axis

            # Determine floor and sensor number from sensor ID
            floor = int(sensor_id[1]) - 1
            node = int(sensor_id[2:])

            displacement_x = 0
            displacement_y = 0
            displacement_z = 0

            if floor >= 0 and node >= 0:
                if axis == 'x':
                    if len(time_data) >= 2:
                        # Update the buffer
                        acceleration_buffers_x[floor].append(value)
                        if len(acceleration_buffers_x[floor]) > acceleration_buffer_size:
                            acceleration_buffers_x[floor].pop(0)
                        
                        filtered_value = highpass_filter(acceleration_buffers_x[floor])[-1]
                        displacement_x = integrate.cumulative_trapezoid([acceleration_buffers_x[floor][-2], filtered_value], time_data[-2:], initial=0)[-1]
                        node_positions[floor, node, 0] += displacement_x * 100

                elif axis == 'y':
                    if len(time_data) >= 2:
                        # Update the buffer
                        acceleration_buffers_y[floor].append(value)
                        if len(acceleration_buffers_y[floor]) > acceleration_buffer_size:
                            acceleration_buffers_y[floor].pop(0)
                        
                        filtered_value = highpass_filter(acceleration_buffers_y[floor])[-1]
                        displacement_y = integrate.cumulative_trapezoid([acceleration_buffers_y[floor][-2], filtered_value], time_data[-2:], initial=0)[-1]
                        node_positions[floor, node, 1] += displacement_y * 100

                elif axis == 'z' and floor == 0:  # Only handle z-axis for the first floor
                    if len(time_data) >= 2:
                        # Update the buffer
                        acceleration_buffers_z[floor].append(value)
                        if len(acceleration_buffers_z[floor]) > acceleration_buffer_size:
                            acceleration_buffers_z[floor].pop(0)
                        
                        filtered_value = highpass_filter(acceleration_buffers_z[floor])[-1]
                        displacement_z = integrate.cumulative_trapezoid([acceleration_buffers_z[floor][-2], filtered_value], time_data[-2:], initial=0)[-1]
                        node_positions[floor, node, 2] += displacement_z 

    # Calculate rotation angles based on the displacement difference between the two x sensors per floor
    for floor in range(num_floors):
        if len(node_positions[floor]) >= 2:
            x_displacement_difference = node_positions[floor, 1, 0] - node_positions[floor, 0, 0]
            rotation_angle = np.degrees(np.arcsin(x_displacement_difference / floor_width))
            rotation_angles[floor] = rotation_angle

# Update node positions when rectangles move
def update_rectangle_node_positions():
    global node_positions
    # Adjust node positions to maintain initial distances
    for floor in range(num_floors - 1):
        for node in range(nodes_per_floor):
            direction = node_positions[floor + 1][node] - node_positions[floor][node]
            current_distance = np.linalg.norm(direction)
            direction_normalized = direction / current_distance
            new_position = node_positions[floor][node] + direction_normalized * initial_distances[floor][node]
            node_positions[floor + 1][node] = new_position

# Update rectangle positions based on node positions
def update_rectangle_positions():
    global x_positions, y_positions, z_positions, node_positions
    for i in range(num_floors):
        x_positions[i] = (node_positions[i][0][0] + node_positions[i][1][0]) / 2 - floor_width / 2
        y_positions[i] = node_positions[i][0][1]  # y_positions are the same for all nodes in a rectangle
        z_positions[i] = (node_positions[i][0][2] + node_positions[i][2][2]) / 2 - floor_depth / 2

    # Update node positions based on rectangle positions
    for i in range(num_floors):
        node_positions[i][0] = [x_positions[i], y_positions[i], z_positions[i]]
        node_positions[i][1] = [x_positions[i] + floor_width, y_positions[i], z_positions[i]]
        node_positions[i][2] = [x_positions[i], y_positions[i], z_positions[i] + floor_depth]
        node_positions[i][3] = [x_positions[i] + floor_width, y_positions[i], z_positions[i] + floor_depth]

# Draw rectangles and lines
def draw_rect(x, y, z, width, height):
    glBegin(GL_QUADS)
    glVertex3f(x, y, z)
    glVertex3f(x + width, y, z)
    glVertex3f(x + width, y, z + height)
    glVertex3f(x, y, z + height)
    glEnd()

def draw_lines(rectangle_positions):
    for i in range(len(rectangle_positions) - 1):
        glColor3f(0, 1, 0)  # Green color
        glBegin(GL_LINES)
        for j in range(4):
            glVertex3f(rectangle_positions[i][j][0], rectangle_positions[i][j][1], rectangle_positions[i][j][2])
            glVertex3f(rectangle_positions[i + 1][j][0], rectangle_positions[i + 1][j][1],
                       rectangle_positions[i + 1][j][2])
        glEnd()

def draw():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glLoadIdentity()

    # Set up perspective projection
    gluPerspective(45, (window_width / window_height), 0.1, 50.0)

    # Set up camera position and orientation
    gluLookAt(5, 10, 10,  # Eye position
              0, 0, 0,  # Look-at position
              0, 1, 0)  # Up direction

    # Draw stacked rectangles
    for i, y in enumerate(y_positions):
        glPushMatrix()
        center_x = x_positions[i] + floor_width / 2
        center_y = y_positions[i] + floor_depth / 2
        center_z = z_positions[i] + floor_depth / 2
        glTranslate(center_x, center_y, center_z)  # Translate to the center of the rectangle
        glRotatef(rotation_angles[i], 0, 1, 0)  # Rotate around the y-axis
        glTranslate(-center_x, -center_y, -center_z)  # Translate back
        glColor3f(1, 0, 0)  # Red color for other rectangles
        draw_rect(x_positions[i], y, z_positions[i], floor_width, floor_depth)
        glPopMatrix()

    # Calculate corner positions for rectangles after rotation
    rectangle_positions = []
    for i in range(num_floors):
        # Get corner positions before rotation
        corners = [
            [x_positions[i], y_positions[i], z_positions[i]],
            [x_positions[i] + floor_width, y_positions[i], z_positions[i]],
            [x_positions[i] + floor_width, y_positions[i], z_positions[i] + floor_depth],
            [x_positions[i], y_positions[i], z_positions[i] + floor_depth]
        ]
        # Rotate corners
        rotated_corners = []
        for corner in corners:
            x, y, z = corner
            x -= x_positions[i] + floor_width / 2  # Translate to origin
            z -= z_positions[i] + floor_depth / 2
            x, z = x * np.sin(np.radians(rotation_angles[i])) - z * np.cos(np.radians(rotation_angles[i])), \
                   x * np.cos(np.radians(rotation_angles[i])) + z * np.sin(np.radians(rotation_angles[i]))  # Rotate
            x += x_positions[i] + floor_width / 2  # Translate back
            z += z_positions[i] + floor_depth / 2
            rotated_corners.append([x, y, z])
        rectangle_positions.append(rotated_corners)

    # Draw lines between corners of adjacent rectangles
    draw_lines(rectangle_positions)

    glfw.swap_buffers(window)

def resize(window, width, height):
    global window_width, window_height
    window_width, window_height = width, height
    glViewport(0, 0, width, height)

async def main():
    global window

    # Initialize the library
    if not glfw.init():
        return

    # Create a windowed mode window and its OpenGL context
    window = glfw.create_window(window_width, window_height, "Stacked Rectangles", None, None)
    if not window:
        glfw.terminate()
        return

    # Make the window's context current
    glfw.make_context_current(window)
    glEnable(GL_DEPTH_TEST)

    # Set the clear color
    glClearColor(0, 0, 0, 1)

    # Set the callback functions
    glfw.set_window_size_callback(window, resize)

    # Start the MQTT client loop
    mqtt_client.loop_start()

    # Main loop
    while not glfw.window_should_close(window):
        # Poll for and process events
        glfw.poll_events()

        # Update node positions based on the received data
        update_rectangle_node_positions()
        update_rectangle_positions()

        # Render here
        draw()

        await asyncio.sleep(1 / 60)  # Wait for 1/60 second before the next frame to match ~60 FPS

    # Stop the MQTT client loop after the window is closed
    mqtt_client.loop_stop()
    glfw.terminate()

if __name__ == "__main__":
    asyncio.run(main())
