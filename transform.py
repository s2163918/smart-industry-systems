import pandas as pd
from nptdms import TdmsFile
from rdflib import Graph, Literal, URIRef, Namespace
from rdflib.namespace import RDF, XSD

# Load the TDMS file
tdms_file_path = r'./t_0001.tdms'  # Correct path to your TDMS file
tdms_file = TdmsFile.read(tdms_file_path)

# Display structure of the file
groups = tdms_file.groups()
data_structure = {group.name: [channel.name for channel in group.channels()] for group in groups}

# Optionally, convert a group to a DataFrame to see the data
group_name = 'Untitled' 
group_data = tdms_file[group_name].as_dataframe()
group_data = group_data.head(50)

# Initialize the graph
g = Graph()

# Define namespaces
SAREF = Namespace("https://w3id.org/saref#")
EXAMPLE = Namespace("http://example.org/")

# Bind namespaces to the graph
g.bind("saref", SAREF)
g.bind("ex", EXAMPLE)

# Define the unit of measurement
unit_uri = URIRef("http://example.org/unit/m_s2")
g.add((unit_uri, RDF.type, SAREF.UnitOfMeasure))
g.add((unit_uri, SAREF.name, Literal("meters per second squared", datatype=XSD.string)))


# Process each row to create RDF triples
for index, row in group_data.iterrows():
    timestamp = Literal(row["Time"], datatype=XSD.dateTime)
    observation = URIRef(f"http://example.org/observation/{index + 1}")

    # Create a measurement for each sensor value
    for sensor, value in row.items():
        if sensor != "Time":
            sensor_uri = URIRef(f"http://example.org/sensor/{sensor}")
            measurement_uri = URIRef(f"http://example.org/measurement/{index + 1}/{sensor}")

            # Add Device (Sensor) triples
            g.add((sensor_uri, RDF.type, SAREF.Device))
            g.add((sensor_uri, SAREF.hasName, Literal(sensor, datatype=XSD.string)))

            # Add Measurement triples
            g.add((measurement_uri, RDF.type, SAREF.Measurement))
            g.add((measurement_uri, SAREF.isMeasurementOf, sensor_uri))
            g.add((measurement_uri, SAREF.hasValue, Literal(value, datatype=XSD.float)))
            g.add((measurement_uri, SAREF.hasTimestamp, timestamp))
            g.add((measurement_uri, SAREF.isMeasuredIn, unit_uri))

            # Link device to measurement
            g.add((sensor_uri, SAREF.makesMeasurement, measurement_uri))

# Serialize the graph in JSON-LD format and write to a file
jsonld_output = g.serialize(format="turtle", indent=4)

# Specify the output file path
output_file_path = 'output_data.ttl'

# Write to file
with open(output_file_path, 'w') as f:
    f.write(jsonld_output)

print(f"Data has been successfully written to {output_file_path}")

